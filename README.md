# hn-articles-nestjs

Technologies: Nestjs, MongoDB, React, Docker, CSS.


# For running the app

- Install <a href='https://docs.mongodb.com/manual/administration/install-community/'>MongoDB</a> 
- Install <a href='https://docs.docker.com/get-docker/'>Docker</a>

-Open terminal and run the following command inside /hn-articles-nestjs folder

```bash
$ docker-compose up
```
