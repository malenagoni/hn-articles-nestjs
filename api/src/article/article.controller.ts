import { Controller, Res, Get, HttpStatus, Put, Param } from '@nestjs/common';
import { Response } from 'express';
import { ArticleService } from './article.service';

@Controller('article')
export class ArticleController {
  constructor(private articleService: ArticleService) {}

  @Get('/')
  async getArticles(@Res() res: Response) {
    const resultsDB = await this.articleService.findAllDataBase();
    return res.json(resultsDB);
  }

  @Get('/save')
  async saveArticles(@Res() res: Response) {
    const data = await this.articleService.APItoDatabase();
    return res.json(data);
  }

  @Put('/update/:articleId')
  async deleted(@Res() res: Response, @Param('articleId') articleId: number) {
    const article = await this.articleService.updateArticle(articleId);
    return res.status(HttpStatus.OK).json({
      message: 'The article was deleted',
      article,
    });
  }
}
