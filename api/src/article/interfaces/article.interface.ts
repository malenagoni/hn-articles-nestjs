import { Document } from 'mongoose';

export interface Article extends Document {
  readonly _id: number;
  readonly created_at: string | Date;
  readonly title: string;
  readonly url: string;
  readonly author: string;
  readonly story_id: number;
  readonly story_title: string;
  readonly story_url: string;
  isDeleted: boolean;
}
