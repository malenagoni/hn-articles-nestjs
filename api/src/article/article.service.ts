import { HttpService, Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { Observable } from 'rxjs';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Article } from './interfaces/article.interface';
import { CreateArticleDTO } from './dto/article.dto';
import { Cron, CronExpression } from '@nestjs/schedule';

@Injectable()
export class ArticleService {
  constructor(
    @InjectModel('Article') private readonly articleModel: Model<Article>,
    private httpService: HttpService,
  ) {}

  async findAllAPI(): Promise<Observable<AxiosResponse<Article[]>> | any> {
    try {
      const response = await this.httpService
        .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
        .toPromise();
      return response.data;
    } catch (e) {
      console.log(e);
    }
  }

  async createArticle(createArticleDTO: CreateArticleDTO): Promise<Article> {
    try {
      const doc = await this.articleModel
        .findOne({ _id: createArticleDTO._id })
        .lean();
      if (!doc) {
        const article = new this.articleModel(createArticleDTO);
        return await article.save();
      } else {
        return;
      }
    } catch (e) {
      console.log(e);
    }
  }

  @Cron(CronExpression.EVERY_HOUR)
  async APItoDatabase(): Promise<Article[]> {
    console.log('holi');
    const data = await this.findAllAPI();
    data?.hits?.map((a: any) => {
      if (!a.story_id) return;
      return this.createArticle({
        _id: a.story_id,
        created_at: a.created_at,
        author: a.author,
        title: a.title,
        url: a.url,
        story_url: a.story_url,
        story_id: a.story_id,
        story_title: a.story_title,
        isDeleted: false,
      });
    });
    return data;
  }

  async findAllDataBase(): Promise<Article[]> {
    try {
      const results = await this.articleModel.find({});
      return results; // status: ok, data: results
    } catch (e) {
      console.log(e); // results: failed, msg: e
    }
  }

  async updateArticle(articleID: number): Promise<any> {
    const article = await this.articleModel.findOne({ _id: articleID });
    if (!article) {
      throw new Error('There is no article with that id in database');
    } else {
      article.isDeleted = true;
      await article.save();
      return article;
    }
  }
}
