import { Schema } from 'mongoose';

export const ArticleSchema = new Schema({
  _id: {
    type: Number,
  },
  created_at: {
    type: String,
    default: Date.now,
  },
  title: String,
  url: String,
  author: String,
  story_id: {
    type: Number,
  },
  story_title: String,
  story_url: String,
  isDeleted: {
    type: Boolean,
    default: false,
  },
});
