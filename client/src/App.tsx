import React from 'react';
import './App.css';
import { Switch, Route } from 'react-router-dom'
import { Layout } from './components/Layout/Layout';

function App() {
  return (
    <div>
      <Switch>
      <Route exact path= '/' component={Layout}/>
      </Switch>
    </div>
  );
}

export default App;
