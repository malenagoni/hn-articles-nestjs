import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { Article } from '../Article/Article'
import { ArticleIF } from '../../interfaces/interfaces'
import '../ArticlesList/ArticlesList.css'

export const API_URL = 'http://localhost:4000/article'

export const ArticlesList = () => {

    const [articles, setArticles] = useState([]);
    const today = new Date().toISOString().split('T')[0]; // Formato YYYY-MM-DD
    const yesterday = new Date(new Date().setDate(new Date().getDate()-1)).toISOString().split('T')[0]; // Formato YYYY-MM-DD
    let date;
    
    useEffect(() => {
        getArticles()
    }, [])
    
    const getArticles = async () => {
            const { data } = await axios.get(`${API_URL}/`); // if status ok .. continue : pop up alert con error
            await data?.sort(function(a:any,b:any){
                return new Date(b.created_at).getTime() - new Date(a.created_at).getTime();
              });
            setArticles(data);
    }

    const formatDate = (created_at:string) => {
        if(created_at.split('T')[0] === today) {
             date = new Intl.DateTimeFormat('en-US', {
                    hour: 'numeric',
                    minute: 'numeric'
            }).format(new Date(created_at));
            return date;
        } else if(created_at.split('T')[0] === yesterday) {
            return date = 'Yesterday';
        } else {
            date = new Intl.DateTimeFormat('en-US', {
                    day: '2-digit',
                    month: 'short'
            }).format(new Date(created_at));
            return date;
        }
    }
    let i = 0;
    return (
        <div className='ArticlesList-Box'>
            {
                articles.length > 0 && articles.map(( a:ArticleIF) => {
                    if ( a.isDeleted === false ) {
                        i++
                        return (
                            <Article 
                                key={`${a._id}${i}`}
                                _id={a._id}
                                title={a.title} 
                                story_title={a.story_title}
                                author={a.author}
                                created_at={formatDate(a.created_at)}
                                url={a.url}
                                story_url={a.story_url}
                                isDeleted={a.isDeleted}
                                articles={articles}
                                setArticles={setArticles}
                                
                            />
                        )
                    }
                    return (<div key={`${a._id}${i}`}></div>)
                })
            }
        </div>
    )
}
