import React from 'react'
import '../Header/Header.css'

export const Header = () => {
    return (
        <div className="Header">
            <div className="Header-title">HN Feed</div>
            <div className="Header-subtitle">We &lt;3 hacker news!</div>
        </div>
    )
}
