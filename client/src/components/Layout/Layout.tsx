import React from 'react'
import { Header } from '../Header/Header'
import { ArticlesList } from '../ArticlesList/ArticlesList'

export const Layout = () => {
    return (
        <div style={{overflowX:'hidden'}}>
            <Header/>
            <ArticlesList/>
        </div>
    )
}
