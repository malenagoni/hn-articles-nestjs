import React from 'react'
import { ArticleIF } from '../../interfaces/interfaces'
import './Article.css'
import trash from '../../png/clipart1087968.png'
import axios from 'axios'
import { API_URL } from '../ArticlesList/ArticlesList'

export const Article = ({_id, title, story_title, author, created_at, url, story_url, articles, setArticles}:ArticleIF) => {

    const link = url  || story_url;
    const name = title || story_title;
    


    const onClick = async () => {
        // eslint-disable-next-line
        const data = await axios.put(`${API_URL}/update/${_id}`);
        const newList = articles?.filter((a:ArticleIF) => a._id !== _id);
        setArticles(newList);
    }

    return (
        <div className='Article-button'>
            <a href={link} target='_blank' style={{textDecoration:'none'}} rel='noreferrer'>
                <div className='Article'>
                    <div className='Article-title-author'>
                        <div className='Article-title'>
                        {name}
                        </div>
                        <div className='Article-author'>
                        - {author} -
                        </div>
                    </div>
                    <div className='Article-time'>
                        {created_at}
                

                    </div>
                </div>
            </a>
            <button onClick={onClick} className='Article-img'>
                <img src={trash} alt='delete' style={{height:'25px', width: '20px', marginTop: '-4px'}}/>
            </button>
        </div>
    )
}
