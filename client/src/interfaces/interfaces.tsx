export interface ArticleIF {
    _id: number;
    title: string | null;
    story_title: string | null ;
    author: string;
    created_at: string;
    url: string | undefined;
    story_url: string | undefined;
    isDeleted: boolean;
    articles: ArticleIF[];
    setArticles: any;
}